# Backend API

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* [Docker](https://docs.docker.com/v17.09/engine/installation/linux/docker-ce/ubuntu/)
* [docker-compose](https://docs.docker.com/compose/install/)
* [maven](https://maven.apache.org/download.cgi)


## Building the images:

mvn clean package docker:build