#!/bin/sh


echo "********************************************************"
echo "Waiting for the database server to start on port $DATABASE_PORT"
echo "********************************************************"
while ! `nc -z db $DATABASE_PORT`; do sleep 3; done
echo "******** Database Server has started "

echo "********************************************************"
echo "Starting Server Dspring.jpa.datasource.url= $DATABASE_URL"
echo "********************************************************"
java -Djava.security.egd=file:/dev/./urandom -Dserver.port=$SERVER_PORT -Dspring.jpa.datasource.url=$DATABASE_URL \
     -Dspring.profiles.active=$PROFILE -jar /usr/local/gbm-challenge/@project.build.finalName@.jar


