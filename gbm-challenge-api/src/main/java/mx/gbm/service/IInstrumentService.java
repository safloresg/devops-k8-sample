/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gbm.service;

import java.util.List;
import mx.gbm.entity.Instrument;

/**
 *
 * @author safloresg sflores@ecaresoft.mx
 */
public interface IInstrumentService {
    
    public List<Instrument> getInstrumentServices();
    
    public void saveInstrument(Instrument instrument);
    
}
