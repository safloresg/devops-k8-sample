/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gbm.service;

import java.util.List;
import mx.gbm.entity.Instrument;
import mx.gbm.repo.InstrumentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author safloresg sflores@ecaresoft.mx
 */
@Service
public class InstrumentService implements IInstrumentService{
    
    @Autowired InstrumentRepo instrumentRepo;

    @Override
    public List<Instrument> getInstrumentServices() {
        
        return (List<Instrument>) instrumentRepo.findAll();
    }
    
    
    public void saveInstrument(Instrument instrument){
        instrumentRepo.save(instrument);
        
    }
    
}
