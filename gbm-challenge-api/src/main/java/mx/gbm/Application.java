/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gbm;

/**
 *
 * @author safloresg sflores@ecaresoft.mx
 */
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
//import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;


/**
 *
 * @author sergio
 */
@SpringBootApplication
public class Application {
    
    public static void main(String[] args){
        
        SpringApplication.run(Application.class, args);    
    }
    
}