/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gbm.repo;

import mx.gbm.entity.Instrument;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author safloresg sflores@ecaresoft.mx
 */

public interface InstrumentRepo extends CrudRepository<Instrument,Long>{
    
}
