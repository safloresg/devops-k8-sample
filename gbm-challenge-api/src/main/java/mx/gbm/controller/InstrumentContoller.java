/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gbm.controller;

import java.util.List;
import javax.inject.Inject;
import mx.gbm.entity.Instrument;
import mx.gbm.service.IInstrumentService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author safloresg sflores@ecaresoft.mx
 */
@RequestMapping(value = "currencies")
@RestController
@CrossOrigin
public class InstrumentContoller {

    //private static final Logger logger = (Logger) LoggerFactory.getLogger(InstrumentContoller.class);

    @Inject IInstrumentService instrumentService;
    
    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Instrument> getInstruments() {
        return instrumentService.getInstrumentServices();
        
    }
    
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public void saveInstrument(Instrument instrument){
        instrumentService.saveInstrument(instrument);
    }

}
