import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Chart } from "react-charts";


const instrumentList = [{
    nombre: 'BTC',
    rendimiento: '.02%',
    venta: '5.00',
    compra: '6.00'
},
{
    nombre: 'ETHERUM',
    rendimiento: '.53%',
    venta: '5.60',
    compra: '6.15'        
}];

const lineChart = (
    // A react-chart hyper-responsively and continuusly fills the available
    // space of its parent element automatically
	<div
    style={{
	width: "100px",
	height: "75px"
    }}
	> <Chart
    data={[
	{
	    label: "Series 1",
            data: [[0, 1], [1, 2], [2, 4], [3, 2], [4, 7]]
        }
    ]}
    axes={[
        { primary: true, type: "linear", position: "bottom" },
        { type: "linear", position: "left" }
    ]}
	/>
	</div>
);



class InstrumentTable extends React.Component {

    render(){
	return (
		<table>
		<thead>
		<tr>
		<th>Nombre</th><th>Rendimiento</th><th>Venta</th><th>Compra</th>		
		</tr>
		</thead>
		<tbody>
		<InstrumentRow />
		</tbody>
		</table>

	);
    }
}

class InstrumentRow extends React.Component {    

    constructor(props){
	super(props);

	this.state = {
	    result: null
	};

	this.fetchCurrencies = this.fetchCurrencies.bind(this);
	this.setCurrencies = this.setCurrencies.bind(this);	
    }

    fetchCurrencies()
    {
	fetch(process.env.REACT_APP_API_URL+'/currencies')
	    .then(response => response.json())
	    .then(result => this.setCurrencies(result))	;
    }

    setCurrencies(result){
	this.setState({result});
    }

    componentDidMount(){
	this.fetchCurrencies();
    }
    
    render(){
	const {result} = this.state;
	console.log(result);

	if (!result) { return null; }
	return(
	    result.map(function(item){
		return(	    <tr>
			    <td>
			    {item.name}
			    </td>
			    <td>			    
			    {lineChart}
			    <span>
			    {item.rendimiento}
			    </span>
			    </td>
			    <td>
			    {item.sale}
			    </td>
			    <td>
			    {item.purchase}
			    </td>
			    </tr>);
	    })
	)
	
    }


}

ReactDOM.render(<InstrumentTable />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
