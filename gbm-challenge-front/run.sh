#!/bin/sh


echo "********************************************************"
echo "Waiting for the API server to start on port $API_PORT"
echo "********************************************************"
while ! `nc -z gbm-challenge $API_PORT`; do sleep 3; done
echo "******** Api  has started ****************************** "

echo "********************************************************"
echo "Starting react application"
echo "********************************************************"

nginx -g 'daemon off;'
