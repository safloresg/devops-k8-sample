# k8-sample

# Backend / API

The API is created using Spring boot + spring Cloud

# Frontend 

The frontend is running in javascript, the React library is used.

# Infrastructure

In the kubernetes folder we can find all the yml templates for the kuberntes objects that are running on AWS EKS.

Currently 3 pods are running (API,Frontend, MariaDB)

Staging Environment:
http://aa3f37fd7344611e9a3b306bc12039d6-1496662892.us-west-2.elb.amazonaws.com




